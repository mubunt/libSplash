# RELEASE NOTES: *libSplash*, a C library to display a 'dynamic' splash screen to be used by any C application.

Functional limitations, if any, of this version are described in the *README.md* file.

**Version 1.2.1**:
  - Updated templates for makefiles (*.make* directory).

**Version 1.2.0**:
  - Added *splash_wait* function.
  - Updated README file.

**Version 1.1.0**:
  - Simplified API: the dimension of the image defines the dimension of the splash screen.
  - Updated information in README file.


**Version 1.0.0**:
  - First version.
