//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libSplash
// a C library to display a 'dynamic' splash screen to be used by any C application
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "splash_internal.h"
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
extern splashDESCR splash;
//------------------------------------------------------------------------------
// LIBRARY FUNCTIONs
//------------------------------------------------------------------------------
void __splash_freeResources( void *buf ) {
	if ( buf != NULL )
		free( buf );
	if ( splash.ximage != NULL )
		XDestroyImage( splash.ximage );
	if ( splash.display != 0) {
		if ( splash.window != 0 ) {
			XUnmapWindow( splash.display, splash.window );
			XDestroyWindow( splash.display, splash.window );
		}
		XCloseDisplay( splash.display );
	}
	splash.ximage = NULL;
	splash.display = NULL;
	splash.data = NULL;
	splash.gc = NULL;
	splash.window = 0;
	splash.screen = 0;
	splash.width = splash.height = 0;
}
//------------------------------------------------------------------------------
Window __splash_createWindow( Display *display, int screen, int x, int y, unsigned int width, unsigned int height ) {
	XSetWindowAttributes attributes;
	attributes.background_pixel = WhitePixel( display, screen );
//	attributes.background_pixel = BlackPixel( display, screen );
	attributes.event_mask = ExposureMask;
	Window window = XCreateWindow(	/* Display*/ 	display,
	                /* Parent */					RootWindow(display, screen),
	                /* X */							x,
	                /* Y */							y,
	                /* Width */						width,
	                /* Heigth */					height,
	                /* Border Width */				0,
	                /* Depth */						CopyFromParent, //DefaultDepth(display, screen),
	                /* Class */						InputOutput,
	                /* Visual */					DefaultVisual(display, screen),
	                /* Value Mask */				CWBackPixel | CWEventMask,
	                /* Attributes */				&attributes );
	if ( ! window ) return window;
	Atom window_type = XInternAtom(	/* Display */	display,
	                   /* Atom name */				"_NET_WM_WINDOW_TYPE",
	                   /* only_if_exist */			false );
	long unsigned value = XInternAtom(	/* Display */	display,
	                      /* Atom name */				"_NET_WM_WINDOW_TYPE_DOCK",
	                      /* only_if_exist */			false );
	XChangeProperty(	/* Display */		display,
	                                        /* Window */					window,
	                                        /* Atom Property */				window_type,
	                                        /* Property type */				XA_ATOM,
	                                        /* Format */					32,
	                                        /* Operation mode */			PropModeReplace,
	                                        /* Property data */				(unsigned char *) &value,
	                                        /* Nb property data elements */	1 );
	return window;
}
//------------------------------------------------------------------------------
XImage *__splash_createImage( Display *display, Visual *visual, char *data, unsigned int width, unsigned int height, int rowbytes ) {
	return XCreateImage(	/* Display */	display,
	                                        /* Visual */					visual,
	                                        /* Depth */						24,
	                                        /* Image Format */				ZPixmap,
	                                        /* Offset */					0,
	                                        /* Data */						data,
	                                        /* Image Width */				width,
	                                        /* Image Height */				height,
	                                        /* Bitmap_pad */				8,
	                                        /* Bytes_per_line */			rowbytes );
}
//------------------------------------------------------------------------------
int __splash_putImage( Display *display, Window window, GC gc, XImage *ximage, unsigned int width, unsigned int height ) {
	return XPutImage(	/* Display */							display,
	        /* Window */										window,
	        /* Graphics context */								gc,
	        /* XImage */										ximage,
	        /* Offset in X from the left edge of the image */	0,
	        /* Offset in Y from the top edge of the image */	0,
	        /* Abscissa of the image in window */				0,
	        /* Ordinate of the image in window */				0,
	        /* Image width */									width,
	        /* Image height */									height );
}
//------------------------------------------------------------------------------
XImage *__splash_getImage( Display *display, Window window, unsigned int width, unsigned int height ) {
	return XGetImage(	/* Display */							display,
	        /* Window */										window,
	        /* Offset in X from the left edge of the image */	0,
	        /* Offset in Y from the top edge of the image */	0,
	        /* Image width */									width,
	        /* Image height */									height,
	        /* Plane mask */									AllPlanes,
	        /* Format */										ZPixmap );
}
//------------------------------------------------------------------------------
// From https://stackoverflow.com/questions/17857575/displaying-png-file-using-xputimage-does-not-work
int __splash_loadPngFile( char *filePNG, unsigned char **data, unsigned int *rowbytes, unsigned int *width, unsigned int *height ) {
	unsigned char header[8];    // 8 is the maximum size that can be checked
	// Open PNG file
	FILE *fptr = fopen(filePNG, "rb");
	if ( fptr == NULL ) return SPLASH_UNABLE_TO_OPEN_FILE;
	size_t unused = fread(header, 1, 8, fptr);
	if ( png_sig_cmp(header, 0, 8)) {
		fclose( fptr );
		return SPLASH_NOT_A_PNG_FILE;
	}
	//  Allocate and initialize a png_struct structure for reading PNG file
	png_structp png = png_create_read_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
	if ( ! png ) {
		fclose( fptr );
		return SPLASH_CANNOT_CREATE_DATA_READ_STRUCTURE;
	}
	// Allocate and initialize a png_info structure
	png_infop info = png_create_info_struct ( png );
	if ( ! info ) {
		png_destroy_read_struct( &png, (png_infopp) NULL, (png_infopp) NULL);
		fclose( fptr );
		return SPLASH_CANNOT_CREATE_INFO_STRUCTURE;
	}
	// Initialize the default input/output functions for the PNG file to standard C streams
	png_init_io( png, fptr );
	// Just to inform that we already read some bytes to test the nature of the file
	png_set_sig_bytes(png, 8);
	// Read the PNG image information
	png_read_info( png, info );
	// Get information from png_info structure
	*width =  png_get_image_width(png, info);
	*height = png_get_image_height(png, info);
	int colortype = png_get_color_type(png, info);
	// Get number of bytes needed to hold a row
	*rowbytes = (unsigned int) png_get_rowbytes( png, info );
	if (colortype == PNG_COLOR_TYPE_RGB) {
		// X hates 24bit images - pad to splashRGBA
		png_set_filler( png, 0xff, PNG_FILLER_AFTER );
		*rowbytes = (*rowbytes * 4) / 3;
	}
	// PNG files store 3 color pixels in red, green, blue order. This code would be used if they are supplied as blue, green, red
	png_set_bgr( png );
	// Size of the image to load
	size_t size = *height * *rowbytes;
	// Allocate memory for image. This will be freed by XDestroyImage
	*data = malloc( size * sizeof (png_byte) );
	if ( *data == NULL ) {
		png_destroy_read_struct( &png, &info, (png_infopp) NULL);
		fclose( fptr );
		return SPLASH_CANNOT_ALLOCATE_DATA_MEMORY;
	}
	// Allocate array of pointers to image rows
	unsigned char **rowPointers = malloc( *height * sizeof(unsigned char *) );
	if (rowPointers == NULL ) {
		png_destroy_read_struct( &png, &info, (png_infopp) NULL);
		free( *data );
		fclose( fptr );
		return SPLASH_CANNOT_ALLOCATE_INTERLEAVED_MEMORY;
	}
	// Initialization of array of pointers to image rows
	png_bytep ptdata = *data;
	for (unsigned int i = 0; i < *height; ++i, ptdata += *rowbytes)
		rowPointers[i] = ptdata;
	// Read the entire image into memory
	png_read_image(png, rowPointers);
	// Finishing a sequential read and close the file
	png_read_end(png, NULL);
	fclose( fptr );
	// free the memory associated with read png_struct
	png_destroy_read_struct( &png, &info, (png_infopp) NULL);
	free (rowPointers);
	return SPLASH_OK;
}
//------------------------------------------------------------------------------
time_t __splash_computeTimestamp( void ) {
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	tm.tm_sec = tm.tm_min = tm.tm_hour = 0;
	return mktime( &tm );
}
//------------------------------------------------------------------------------
