//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libSplash
// a C library to display a 'dynamic' splash screen to be used by any C application
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "splash_internal.h"
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
splashDESCR splash;
//------------------------------------------------------------------------------
// LIBRARY FUNCTION
//------------------------------------------------------------------------------
int splash_create( char *fileimage ) {
	//--- Initializations
	memset( &splash, 0, sizeof(splash));
	splash.timestamp = __splash_computeTimestamp();

	//--- Allocate memory for image and read the image from file
	unsigned int rowbytes = 0;
	unsigned int width_ = 0;
	unsigned int height_ = 0;
	int n = __splash_loadPngFile( fileimage, &splash.data, &rowbytes, &width_, &height_ );
	if ( n != SPLASH_OK ) return n;
	splash.width = width_;
	splash.height = height_;

	//--- Open a connection with the X server
	splash.display = XOpenDisplay( NULL );
	if ( ! splash.display ) return SPLASH_UNABLE_TO_OPEN_DISPLAY;

	//--- Get the default screen and its size
	splash.screen = DefaultScreen( splash.display );
	Screen *pScreen = ScreenOfDisplay( splash.display, splash.screen );

	//--- Compute splash screen coordinates
	splash.x = ( pScreen->width - (int) splash.width ) / 2;
	splash.y = ( pScreen->height - (int) splash.height ) / 2;

	//--- Create the splash window
	splash.window = __splash_createWindow( splash.display, splash.screen, splash.x, splash.y, splash.width, splash.height );
	if ( ! splash.window ) return SPLASH_UNABLE_TO_CREATE_WINDOW;

	//--- Map and show splash window
	XSelectInput( splash.display, splash.window, ExposureMask | KeyPressMask);
	XMapWindow( splash.display, splash.window );
	XFlush( splash.display );

	//--- Allocate a new GC (graphics context) for drawing in the window.
	splash.gc = XCreateGC(	/* Display*/ 		splash.display,
	            /* Window */		splash.window,
	            /* Value Mask */	0,
	            /* Attributes */	NULL );
	if ( ! splash.gc ) return SPLASH_UNABLE_TO_CREATE_GC;

	//--- Create the XImage
	splash.ximage = __splash_createImage( splash.display, DefaultVisualOfScreen( pScreen ), (char *) splash.data, splash.width, splash.height, (int) rowbytes );
	if ( splash.ximage == NULL )  {
		__splash_freeResources( splash.data );
		return SPLASH_UNABLE_TO_CREATE_IMAGE;
	}

	//--- Draw the XImage to the window
	if (0 != __splash_putImage( splash.display, splash.window, splash.gc, splash.ximage, splash.width, splash.height )) {
		__splash_freeResources( NULL );
		return SPLASH_UNABLE_TO_DRAW_IMAGE;
	}

	//--- Flush output buffer
	XFlush(	splash.display );

	XEvent event;
	bool exit = false;
	while ( ! exit ) {
		XNextEvent( splash.display, &event);
		if (event.type == Expose)
			__splash_putImage( splash.display, splash.window, splash.gc, splash.ximage, splash.width, splash.height );
		exit = true;
	}
	return SPLASH_OK;
}
//------------------------------------------------------------------------------
