//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libSplash
// a C library to display a 'dynamic' splash screen to be used by any C application
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "splash_internal.h"
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
extern splashDESCR splash;
//------------------------------------------------------------------------------
// LIBRARY FUNCTION
//------------------------------------------------------------------------------
int splash_ack(  ) {
	TIMESTAMP_CHECKING

	int i = XGrabPointer(splash.display, splash.window, False, ButtonPressMask, GrabModeAsync, GrabModeAsync, None, None, CurrentTime);
	if(i != GrabSuccess) {
		__splash_freeResources( NULL );
		return SPLASH_CANT_GRAB_THE_MOUSE;
	}
	XEvent report;
	XButtonEvent *xb = (XButtonEvent *)&report;

	int topleftcorner_x = splash.x;
	int topleftcorner_y = splash.y;
	int toprightcorner_x = splash.x + (int) splash.width - 1;
	int bottomleftcorner_y = splash.y + (int) splash.height - 1;
	while (1) {
		XAllowEvents(splash.display, SyncPointer, CurrentTime);
		XWindowEvent(splash.display, splash.window, ButtonPressMask, &report);
		if (report.type == ButtonPress) {
			if (xb->x_root < topleftcorner_x || xb->x_root > toprightcorner_x ||
			        xb->y_root < topleftcorner_y || xb->y_root > bottomleftcorner_y)
				return SPLASH_OK;
		}
	}
}
//------------------------------------------------------------------------------
