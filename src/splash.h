//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libSplash
// a C library to display a 'dynamic' splash screen to be used by any C application
//------------------------------------------------------------------------------
#ifndef SPLASH_H
#define SPLASH_H
//------------------------------------------------------------------------------
// DIAGNOSTIC DEFINITIONS
//------------------------------------------------------------------------------
#define SPLASH_OK										0
#define SPLASH_WRONG_USE_OF_SPLASH_API					1
#define SPLASH_UNABLE_TO_OPEN_DISPLAY 					2
#define SPLASH_UNABLE_TO_CREATE_WINDOW 					3
#define SPLASH_UNABLE_TO_CREATE_GC						4
#define SPLASH_UNABLE_TO_OPEN_FILE						5
#define SPLASH_CANNOT_CREATE_DATA_READ_STRUCTURE		6
#define SPLASH_CANNOT_CREATE_INFO_STRUCTURE				7
#define SPLASH_CANNOT_ALLOCATE_DATA_MEMORY				8
#define SPLASH_CANNOT_ALLOCATE_INTERLEAVED_MEMORY		9
#define SPLASH_UNABLE_TO_CREATE_IMAGE					10
#define SPLASH_UNABLE_TO_READ_FILE						11
#define SPLASH_UNABLE_TO_DRAW_IMAGE						12
#define SPLASH_CANNOT_DRAW_STRING						13
#define SPLASH_CANNOT_DRAW_LINE							14
#define SPLASH_UNABLE_TO_ALLOCATE_COLOR					15
#define SPLASH_UNABLE_TO_ALLOCATE_FONT					16
#define SPLASH_NOT_A_PNG_FILE 							17
#define SPLASH_INCORRECT_NUMBER_OF_INITIALIZATION_STEPS	18
#define SPLASH_INCORRECT_PROGRESS_BAR_ORDINATE			19
#define SPLASH_INCORRECT_PROGRESS_STEP_ORDINATE			20
#define SPLASH_INCORRECT_STEP_NUMBER					21
#define SPLASH_INCORRECT_STRING_ABSCISSA				22
#define SPLASH_INCORRECT_STRING_ORDINATE				23
#define SPLASH_CANT_GRAB_THE_MOUSE						24
//------------------------------------------------------------------------------
// STRUCTURE DEFINITIONS
//------------------------------------------------------------------------------
typedef const char *splashFONT;

typedef struct splashRGB {
	unsigned short red;
	unsigned short green;
	unsigned short blue;
} splashRGB;
//------------------------------------------------------------------------------
// ROUTINES DEFINITIONS
//------------------------------------------------------------------------------
extern int 	splash_create( char * );
extern int 	splash_destroy( void );
extern int 	splash_write( int, int, const char *, splashRGB, splashFONT );
extern int 	splash_progress( unsigned int, splashRGB, splashFONT, int, int );
extern int 	splash_progressStep( int, char * );
extern void	splash_perror( int );
extern int 	splash_ack( void );
//------------------------------------------------------------------------------
#endif	// SPLASH_H
