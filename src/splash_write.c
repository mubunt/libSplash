//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libSplash
// a C library to display a 'dynamic' splash screen to be used by any C application
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "splash_internal.h"
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
extern splashDESCR splash;
//------------------------------------------------------------------------------
// LIBRARY FUNCTION
//------------------------------------------------------------------------------
int splash_write( int x, int y, const char *string, splashRGB color, splashFONT font ) {
	TIMESTAMP_CHECKING

	if (x <= 0 || x > (int) splash.width) {
		__splash_freeResources( NULL );
		return SPLASH_INCORRECT_STRING_ABSCISSA;
	}
	if (y <= 0 || y > (int) splash.height) {
		__splash_freeResources( NULL );
		return SPLASH_INCORRECT_STRING_ORDINATE;
	}
	XGCValues gr_values;
	XFontStruct *fontinfo = XLoadQueryFont(splash.display, font);
	if ( ! fontinfo) {
		__splash_freeResources( NULL );
		return SPLASH_UNABLE_TO_ALLOCATE_FONT;
	}
	gr_values.font = fontinfo->fid;
	GC gc = XCreateGC(	/* Display*/ 		splash.display,
	                                        /* Window */		splash.window,
	                                        /* Value Mask */	GCFont,
	                                        /* Attributes */	&gr_values );
	if ( ! gc ) {
		__splash_freeResources( NULL );
		return SPLASH_UNABLE_TO_CREATE_GC;
	}
	XSetForeground(splash.display, gc, (long unsigned int) (color.blue + (color.green<<8) + (color.red<<16)));

	if (0 != XDrawString( splash.display, splash.window, gc, x, y, string, (int) strlen(string) )) {
		__splash_freeResources( NULL );
		return SPLASH_CANNOT_DRAW_STRING;
	}
	XFlush(	splash.display );

	splash.ximage = __splash_getImage( splash.display, splash.window, splash.width, splash.height );
	if ( ! splash.ximage) {
		__splash_freeResources( NULL );
		return SPLASH_UNABLE_TO_DRAW_IMAGE;
	}
	return SPLASH_OK;
}
//------------------------------------------------------------------------------
