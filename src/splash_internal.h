//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libSplash
// a C library to display a 'dynamic' splash screen to be used by any C application
//------------------------------------------------------------------------------
#ifndef SPLASHINT_H
#define SPLASHINT_H
//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <X11/Xlib.h>
#include <png.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "splash.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define PROGRESSBAR_WIDTH	10
#define PROGRESS_MARGIN		10

#define TIMESTAMP_CHECKING	if ( splash.timestamp != __splash_computeTimestamp()) { \
								__splash_freeResources( NULL ); \
								return SPLASH_WRONG_USE_OF_SPLASH_API; \
							}
//------------------------------------------------------------------------------
// STRUCTURE
//------------------------------------------------------------------------------
typedef struct splashDESCR {
	time_t			timestamp;
	Display 		*display;
	int 			screen;
	XImage 			*ximage;
	Window 			window;
	GC 				gc;
	int				x;
	int				y;
	unsigned int 	width;
	unsigned int 	height;
	unsigned int	steps;
	GC				progressgc;
	int 			yBar;
	int 			yLabel;
	unsigned char 	*data;
} splashDESCR;
//------------------------------------------------------------------------------
// ROUTINES DEFINITIONS
//------------------------------------------------------------------------------
extern void		__splash_freeResources( void * );
extern Window	__splash_createWindow( Display *, int, int, int, unsigned int, unsigned int );
extern XImage	*__splash_createImage( Display *, Visual *, char *, unsigned int, unsigned int, int );
extern int 		__splash_putImage( Display *, Window, GC, XImage *, unsigned int, unsigned int );
extern XImage 	*__splash_getImage( Display *, Window, unsigned int, unsigned int );
extern int 		__splash_loadPngFile( char *, unsigned char **, unsigned int *, unsigned int *, unsigned int * );
extern time_t	__splash_computeTimestamp( void );
//------------------------------------------------------------------------------
#endif	// SPLASHINT_H
