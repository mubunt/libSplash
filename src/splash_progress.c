//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libSplash
// a C library to display a 'dynamic' splash screen to be used by any C application
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "splash_internal.h"
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
extern splashDESCR splash;
//------------------------------------------------------------------------------
// LIBRARY FUNCTION
//------------------------------------------------------------------------------
int splash_progress( unsigned int numberOfSteps, splashRGB color, splashFONT font, int yBar, int yLabel ) {
	TIMESTAMP_CHECKING

	if ( numberOfSteps <= 0) {
		__splash_freeResources( NULL );
		return SPLASH_INCORRECT_NUMBER_OF_INITIALIZATION_STEPS;
	}
	if (yBar <= 0 || yBar > (int) splash.height) {
		__splash_freeResources( NULL );
		return SPLASH_INCORRECT_PROGRESS_BAR_ORDINATE;
	}
	if (yLabel <= 0 || yLabel > (int) splash.height) {
		__splash_freeResources( NULL );
		return SPLASH_INCORRECT_PROGRESS_STEP_ORDINATE;
	}
	splash.steps = numberOfSteps;
	splash.yBar = yBar;
	splash.yLabel = yLabel;

	XGCValues gr_values;
	XFontStruct *fontinfo = XLoadQueryFont(splash.display, font);
	if ( ! fontinfo) {
		__splash_freeResources( NULL );
		return SPLASH_UNABLE_TO_ALLOCATE_FONT;
	}
	gr_values.font = fontinfo->fid;
	splash.progressgc = XCreateGC(	/* Display*/ 		splash.display,
	                    /* Window */		splash.window,
	                    /* Value Mask */	GCFont,
	                    /* Attributes */	&gr_values );
	if ( ! splash.progressgc ) {
		__splash_freeResources( NULL );
		return SPLASH_UNABLE_TO_CREATE_GC;
	}
	XSetForeground(splash.display, splash.progressgc, (long unsigned int) (color.blue + (color.green<<8) + (color.red<<16)));
	// Define the fill style for the GC.
	XSetFillStyle( splash.display, splash.progressgc, FillSolid );
	//--- Flush output buffer and wait until all requests have been received and processed by the X server.
	XSync( splash.display, false );
	return SPLASH_OK;
}
//------------------------------------------------------------------------------
int splash_progressStep( int stepNumber, char *stepLabel) {
	TIMESTAMP_CHECKING

	if ( stepNumber <= 0 || stepNumber > (int) splash.steps ) {
		__splash_freeResources( NULL );
		return SPLASH_INCORRECT_STEP_NUMBER;
	}
	int xBorderProgress = PROGRESS_MARGIN;
	int xEndProgress = (int) splash.width - PROGRESS_MARGIN;
	//--- Draw the original XImage to the window
	if (0 != __splash_putImage( splash.display, splash.window, splash.gc, splash.ximage, splash.width, splash.height )) {
		__splash_freeResources( NULL );
		return SPLASH_UNABLE_TO_DRAW_IMAGE;
	}
	int progressBarLength = xEndProgress - xBorderProgress;
	int progressBarUnit = progressBarLength / (int) splash.steps;
	int progressBarEnd = xBorderProgress + (stepNumber - 1) * progressBarUnit;
	if ( stepNumber != 1) {
		//--- Define the style of lines that will be drawn using this GC.
		XSetLineAttributes( splash.display, splash.progressgc, PROGRESSBAR_WIDTH, LineSolid, CapButt, JoinBevel);
		//--- Draw progress bar (actions done)
		XDrawLine( splash.display, splash.window, splash.progressgc, xBorderProgress, splash.yBar, progressBarEnd, splash.yBar);
	}
	int progressBarEndNew = progressBarEnd + progressBarUnit;
	if (stepNumber == (int) splash.steps)
		progressBarEndNew = xEndProgress;
	//--- Define the style of lines that will be drawn using this GC.
	XSetLineAttributes( splash.display, splash.progressgc, PROGRESSBAR_WIDTH, LineOnOffDash, CapButt, JoinBevel);
	//--- Draw progress bar (on going action)
	XDrawLine( splash.display, splash.window, splash.progressgc, progressBarEnd, splash.yBar, progressBarEndNew, splash.yBar);

	//--- Draw progress message
	int n = XDrawString( splash.display, splash.window, splash.progressgc, xBorderProgress, splash.yLabel, stepLabel, (int) strlen(stepLabel) );
	//--- Flush output buffer
	XFlush(	splash.display );
	return (n == 0 ? SPLASH_OK : SPLASH_CANNOT_DRAW_STRING);
}
//------------------------------------------------------------------------------
