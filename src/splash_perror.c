//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libSplash
// a C library to display a 'dynamic' splash screen to be used by any C application
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "splash_internal.h"
//------------------------------------------------------------------------------
// LIBRARY FUNCTION
//------------------------------------------------------------------------------
void splash_perror( int status ) {
	fprintf(stderr, "libSplash: ");
	switch (status) {
	case SPLASH_OK:
		fprintf(stderr, "OK.");
		break;
	case SPLASH_WRONG_USE_OF_SPLASH_API:
		fprintf(stderr, "Wrong use of Splash API.");
		break;
	case SPLASH_UNABLE_TO_OPEN_DISPLAY:
		fprintf(stderr, "Cannot open display.");
		break;
	case SPLASH_UNABLE_TO_CREATE_WINDOW:
		fprintf(stderr, "(internal error) Cannot create X window.");
		break;
	case SPLASH_UNABLE_TO_CREATE_GC:
		fprintf(stderr, "(internal error) Cannot create graphic context.");
		break;
	case SPLASH_UNABLE_TO_OPEN_FILE:
		fprintf(stderr, "Cannot open file.");
		break;
	case SPLASH_CANNOT_CREATE_DATA_READ_STRUCTURE:
		fprintf(stderr, "(internal error) Cannot read PNG data structure.");
		break;
	case SPLASH_CANNOT_CREATE_INFO_STRUCTURE:
		fprintf(stderr, "(internal error) Cannot read PNG information structure.");
		break;
	case SPLASH_CANNOT_ALLOCATE_DATA_MEMORY:
		fprintf(stderr, "(internal error) Cannot allocate data memory for PNG image.");
		break;
	case SPLASH_CANNOT_ALLOCATE_INTERLEAVED_MEMORY:
		fprintf(stderr, "(internal error) Cannot allocate interleaved memory for PNG image.");
		break;
	case SPLASH_UNABLE_TO_CREATE_IMAGE:
		fprintf(stderr, "(internal error) Cannot create image.");
		break;
	case SPLASH_UNABLE_TO_READ_FILE:
		fprintf(stderr, "cannot read file.");
		break;
	case SPLASH_UNABLE_TO_DRAW_IMAGE:
		fprintf(stderr, "(internal error) Cannot draw image.");
		break;
	case SPLASH_CANNOT_DRAW_STRING:
		fprintf(stderr, "(internal error) Cannot draw text.");
		break;
	case SPLASH_CANNOT_DRAW_LINE:
		fprintf(stderr, "(internal error) Cannot draw line.");
		break;
	case SPLASH_UNABLE_TO_ALLOCATE_COLOR:
		fprintf(stderr, "(internal error) Cannot allocate color.");
		break;
	case SPLASH_UNABLE_TO_ALLOCATE_FONT:
		fprintf(stderr, "(internal error) Cannot allocate font.");
		break;
	case SPLASH_NOT_A_PNG_FILE:
		fprintf(stderr, "Specified file is not a PNG one.");
		break;
	case SPLASH_INCORRECT_NUMBER_OF_INITIALIZATION_STEPS:
		fprintf(stderr, "Incorrect number of initialization steps.");
		break;
	case SPLASH_INCORRECT_PROGRESS_BAR_ORDINATE:
		fprintf(stderr, "Progress bar ordinate not compliant with splash screen height.");
		break;
	case SPLASH_INCORRECT_PROGRESS_STEP_ORDINATE:
		fprintf(stderr, "Step label ordinate not compliant with splash screen height.");
		break;
	case SPLASH_INCORRECT_STEP_NUMBER:
		fprintf(stderr, "Incorrect step number.");
		break;
	case SPLASH_INCORRECT_STRING_ABSCISSA:
		fprintf(stderr, "Label abscissa not compliant with splash screen width.");
		break;
	case SPLASH_INCORRECT_STRING_ORDINATE:
		fprintf(stderr, "Label ordinate not compliant with splash screen height.");
		break;
	case SPLASH_CANT_GRAB_THE_MOUSE:
		fprintf(stderr, "(internal error) Cannot grab the mouse.");
		break;
	default:
		fprintf(stderr, "Unknown error.");
		break;
	}
	fprintf(stderr, "\n");
}
//------------------------------------------------------------------------------
