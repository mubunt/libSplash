//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "splash.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define font1				"-adobe-helvetica-bold-o-normal--0-0-100-100-p-0-iso10646-1"
#define font2				"-adobe-new century schoolbook-bold-i-normal--0-0-100-100-p-0-iso10646-1"
#define font3				"-misc-fixed-bold-r-normal--13-100-100-100-c-70-iso8859-1"

#define NUMBERofSTEPS		6
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
typedef struct sThreadMessage {
	int step;
	char label[80];
} sThreadMessage;
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
// Refer to https://en.wikipedia.org/wiki/X11_color_names
// or https://fr.wikipedia.org/wiki/Noms_de_couleur_X11
static splashRGB MediumBlue		= { 0, 0, 205 };
static splashRGB MediumSeaGreen	= { 60, 179, 113 };
static splashRGB White 			= { 255, 255, 255 };
static splashRGB Crimson		= { 220, 20, 60 };

static pthread_mutex_t mutex 	= PTHREAD_MUTEX_INITIALIZER;
static sThreadMessage message 	= { 0, "" };
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
// Thread to simulate the loading of an application
void *applicationLoading( ) {
	for (int i = 1; i <= NUMBERofSTEPS; i++) {
		pthread_mutex_lock(&mutex);
		message.step = i;
		switch (i) {
		case 1:
			strcpy(message.label, "Loading fonts...");
			break;
		case 2:
			strcpy(message.label, "Loading Colours...");
			break;
		case 3:
			strcpy(message.label, "Loading Templates...");
			break;
		case 4:
			strcpy(message.label, "Loading Models...");
			break;
		case 5:
			strcpy(message.label, "Loading European Data...");
			break;
		case 6:
			strcpy(message.label, "Loading Chinese Data...");
			break;
		}
		pthread_mutex_unlock(&mutex);
		sleep( 3 );
	}
	pthread_exit(NULL);
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main (int argc, char *argv[]) {
	if ( argc != 3 ) {
		fprintf( stderr, "USAGE: %s <display type> <png file>\n", argv[0] );
		fprintf( stderr, "		display type : 1 --> application loading example\n");
		fprintf( stderr, "		               2 --> application information display example\n\n");
		return( EXIT_FAILURE );
	}

	int choice = atoi(argv[1]);
	int n;
	switch (choice) {
	case 1:
		//--- Splash screen creation and display (static image)
		n = splash_create( argv[2] );
		fprintf( stderr, "--> splash_create - ");
		splash_perror( n );
		if ( n != SPLASH_OK ) return( EXIT_FAILURE );
		sleep(2);

		//--- Adding og dynamic information and display
		n = splash_write( 57, 140, "Version 1.2.1 - June 2020", MediumBlue, font1 );
		fprintf(stderr, "--> splash_write - ");
		splash_perror( n );
		if ( n != SPLASH_OK ) return( EXIT_FAILURE );
		sleep(1);
		n = splash_write( 57, 160, "Thanks for using libSplash :-)", MediumSeaGreen, font2 );
		fprintf(stderr, "--> splash_write - ");
		splash_perror( n );
		if ( n != SPLASH_OK ) return( EXIT_FAILURE );
		sleep(1);

		//--- Initialization of progress information
		//n = splash_progress( NUMBERofSTEPS, Crimson, font3, 350, 370);
		n = splash_progress( NUMBERofSTEPS, White, font3, 350, 370);
		fprintf(stderr, "--> splash_progress - ");
		splash_perror( n );
		if ( n != SPLASH_OK ) return( EXIT_FAILURE );
		//--- Simulation of an application loading with a thread
		pthread_t thread;
		if ( pthread_create( &thread, NULL, applicationLoading, (void *)"" )) {
			perror("pthread_create");
			splash_destroy();
			return( EXIT_FAILURE );
		}

		int previousstep = message.step;
		while (true) {
			n = SPLASH_OK;
			pthread_mutex_lock(&mutex);
			if ( message.step != previousstep) {
				n = splash_progressStep( message.step, message.label );
				fprintf(stderr, "--> splash_progress - ");
				splash_perror( n );
			}
			previousstep = message.step;
			pthread_mutex_unlock(&mutex);
			if ( n != SPLASH_OK ) return( EXIT_FAILURE );
			if ( previousstep == NUMBERofSTEPS ) break;
		}
		void *c;
		if ( pthread_join( thread, &c )) {
			perror("pthread_join");
			splash_destroy();
			return( EXIT_FAILURE );
		}
		//--- Resources release
		n = splash_destroy();
		fprintf(stderr, "--> splash_destroy - ");
		splash_perror( n );
		return( EXIT_SUCCESS );

	case 2:
		//--- Splash screen creation and display (static image)
		n = splash_create( argv[2] );
		fprintf( stderr, "--> splash_create - ");
		splash_perror( n );
		if ( n != SPLASH_OK ) return( EXIT_FAILURE );
		//--- Waiting user's acknowledge
		n = splash_ack( );
		fprintf(stderr, "--> splash_ack - ");
		splash_perror( n );
		//--- Resources release
		n = splash_destroy();
		fprintf(stderr, "--> splash_destroy - ");
		splash_perror( n );
		return( EXIT_SUCCESS );

	default:
		fprintf( stderr, "ERROR: Wrong display type!!!\n\n");
		return( EXIT_FAILURE );
	}
}
// Check if fonts exist with "xlsfonts" command.
//------------------------------------------------------------------------------
