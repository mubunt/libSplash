 # *libSplash*, a C library to display a 'dynamic' splash screen to be used by any C application.

The **libSplash** library allows any C application (graphic or not) to display a splash screen from an image in PNG format. It also gives the possibility of dynamically writing text on the image with a color, font and position of its choice; above all it allows (it's a bit the purpose of splash screens, isn't it!) to display the loading status of the application using a progress bar and labels. The color, font and position of this information are also defined by the application.

**Example 1**:
![libSplash](README_images/libSplash01.png  "Example 1")

**Example 2**:
![libSplash](README_images/libSplash02.png  "Example 2")

## LICENSE
**libSplash** is covered by the GNU General Public License (GPL) version 3 and above.

## API DEFINITION
### splash_creat
*Declaration:*

```C
#include "splash.h"
int splash_create( char *fileimage );
```
*Description:*
The **splash_create** function creates the splash screen, that means creates a window without any decoration and display inside the spedified image, all in the middle of the screen.
- **fileimage** specifies the name of the PNG file which is the basis for the splash screen. The dimension of the splash screen will be equal to the image ones.

*Return value:*
0 (*SPLASH_OK*) if everything went well otherwise the number of the detected error. The error description can then be displayed with the **splash_perror** function.

*Example:*
```C
int n = splash_create( pngfilename );
```

### splash_destroy
*Declaration:*
```C
#include "splash.h"
int splash_destroy( void );
```
*Description:*
The **splash_destroy** function destroys the splash screen as well as all internal resources created for it.

*Return value:*
0 (*SPLASH_OK*) if everything went well otherwise the number of the detected error. The error description can then be displayed with the **splash_perror** function.

*Example:*
```C
n = splash_destroy();
```

### splash_write
*Declaration:*
```C
#include "splash.h"
int splash_write( int x, int y, const char *string, splashRGB color, splashFONT font );
```
*Description:*
The **splash_write** function displays a character string at the specified coordinates.
- **x** and **y** specify the x and y coordinates (in pixel), which are the top-left outside corner of the window's borders and are relative to the inside of the parent window's borders. 
- **string** specifies the string of characters to display.
- **color** specifies the RGB color of the displayed string.
- **font** specifies the string name of the font to be used to display the string.

*Return value:*
0 (*SPLASH_OK*) if everything went well otherwise the number of the detected error. The error description can then be displayed with the **splash_perror** function.

*Example:*
```C
#define font1			"-adobe-helvetica-bold-o-normal--0-0-100-100-p-0-iso10646-1"
splashRGB MediumBlue	= { 0, 0, 205 };
...
n = splash_write( 57, 140, "Version 1.2.1 - June 2020", MediumBlue, font1 );
```

### splash_ack
Declaration:*
```C
#include "splash.h"
int splash_ack( );
```
*Description:*
The **splash_ack** function allows to wait an acknowledgment from the user before cleairing the splach screen. The acknowledgment is done by clicking with a mouse button (any of them) **outside** of the splash screen.

*Return value:*
0 (*SPLASH_OK*) if everything went well. User has acknowledged the display.

*Example:*
```C
n = splash_ack( );
```

### splash_progress
*Declaration:*
```C
#include "splash.h"
int splash_progress( unsigned int numberOfSteps, splashRGB color, splashFONT font, int yBar, int yLabel );
```
*Description:*
The **splash_progress** function initializes the loading progress display phase.
- **numberOfSteps** specifies the number of steps  that make up the load.
- **color** specifies the RGB color of the progress bar and labels.
- **font** specifies the string name of the font to be used to display labels.
- **yBar** specifies the y coordinate (in pixel) of the progress bar.
- **yLabel** specifies the y coordinate (in pixel) of labels.

*Return value:*
0 (*SPLASH_OK*) if everything went well otherwise the number of the detected error. The error description can then be displayed with the **splash_perror** function.

*Example:*
```C
#define font3				"-misc-fixed-bold-r-normal--13-100-100-100-c-70-iso8859-1"
#define NUMBERofSTEPS		6
splashRGB White 			= { 255, 255, 255 };
...
n = splash_progress( NUMBERofSTEPS, White, font3, 350, 370);
```

### splash_progressStep
*Declaration:*
```C
#include "splash.h"
int splash_progressStep( int stepNumber, char *stepLabel );
```
*Description:*
The **splash_progressStep** function defines the name of each loading step.
- **stepNumber** specified the step number.
- **stepLabel** specified the name of this step.

*Return value:*
0 (*SPLASH_OK*) if everything went well otherwise the number of the detected error. The error description can then be displayed with the **splash_perror** function.

*Example:*
```C
n = splash_progressStep( 3, "Loading Templates..." );
```

### splash_perror
*Declaration:*
```C
#include "splash.h"
void splash_perror( int status );
```
*Description:*
The **splash_perror** function prints a **libSplash** error message.
- **status** is a error number delivered by a **libSplash** function.

*Return value:*
none

*Example:*
```C
int n = splash_create( pngfilename );
fprintf( stderr, "--> splash_create - ");
splash_perror( n );
```

## EXAMPLE OF USE
The source file *libSplash/examples/example_splash.c* gives an example of the use of the **libSplash** API.


## STRUCTURE OF THE APPLICATION
This section walks you through **libSplash**'s structure. Once you understand this structure, you will easily find your way around in **libSplash**'s code base.

``` bash
$ yaTree
./                        # Application level
├── README_images/        # Images for documentation
│   ├── libSplash01.png   # 
│   └── libSplash02.png   # 
├── examples/             # Examples of libSplash usage
│   ├── Makefile          # Makefile
│   ├── example_splash.c  # Example of 'libSplash' usage
│   ├── splash.png        # PNG file for the libSplash' usage example
│   ├── splash2.png       # PNG file for the libSplash' usage example
│   └── splash3.png       # 
├── src/                  # Source directory
│   ├── Makefile          # Makefile
│   ├── splash.h          # Splash Header file
│   ├── splash_create.c   # 'splash_create' source implementation
│   ├── splash_destroy.c  # 'splash_destroy' source implementation
│   ├── splash_funcs.c    # Internal routines
│   ├── splash_internal.h # Internal header file
│   ├── splash_perror.c   # 'splash_perror' source implementation
│   ├── splash_progress.c # 'splash_progress' and 'splash_progressStep' source implementation
│   ├── splash_ack.c      # 'splash_ack' source implementation
│   └── splash_write.c    # 'splash_write' source implementation
├── COPYING.md            # GNU General Public License markdown file
├── LICENSE.md            # License markdown file
├── Makefile              # Makefile
├── README.md             # ReadMe markdown file
├── RELEASENOTES.md       # Release Notes markdown file
└── VERSION               # Version identification text file

3 directories, 23 files
$ 
```

## HOW TO BUILD THIS APPLICATION IN DEBUG MODE (including example program)
```Shell
$ cd libSplash
$ make clean all
```
To run the example program:
```Shell
$ ./linux/example_splash 
USAGE: ./linux/example_splash <display type> <png file>
		display type : 1 --> application loading example
		               2 --> application information display example

$ ./linux/example_splash 1 examples/splash.png 
--> splash_create - libSplash: OK.
--> splash_write - libSplash: OK.
--> splash_write - libSplash: OK.
--> splash_progress - libSplash: OK.
--> splash_progress - libSplash: OK.
--> splash_progress - libSplash: OK.
--> splash_progress - libSplash: OK.
--> splash_progress - libSplash: OK.
--> splash_progress - libSplash: OK.
--> splash_progress - libSplash: OK.
--> splash_destroy - libSplash: OK.
$ ./linux/example_splash 2 examples/splash3.png 
--> splash_create - libSplash: OK.
--> splash_wait - libSplash: OK.
--> splash_destroy - libSplash: OK.
$ 
```

*Note*: the *splash3.png* file is a screenshot coming from *www.20minutes.fr* Web site: © Copyright 20 Minutes.

## HOW TO BUILD, INSTALL AND USE THIS APPLICATION IN RELEASE MODE
```Shell
$ cd libSplash
$ make release
    # Library generated with -O2 option and the header file, are respectively installed in $LIB_DIR and $INC_DIR directories (defined at environment level).
```

## NOTES

## SOFTWARE REQUIREMENTS
- For development and usage, libX11 and libpng (=> link with -lX11 -lpng).
- For example program, libpthread (=> link with -lpthread).
- Developped and tested on XUBUNTU 20.04, GCC v9.3.0.

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***